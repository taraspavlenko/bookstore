﻿using BookStore.Services.Services.Interfaces;
using BookStore.Services.Services.Views.BookViews;
using BookStore.Services.Views.BookViews;
using Microsoft.AspNetCore.Mvc;
using System.Threading.Tasks;

namespace BookStore.API.Controllers
{
    [Route("api/[controller]/[action]")]
    public class BookController : Controller
    {
        private IBookService _bookService;
        public BookController(IBookService bookService)
        {
            _bookService = bookService;
        }
        [HttpPost]
        public async Task<IActionResult> Create([FromBody] CreateBookView viewModel)
        {
            if (viewModel == null)
            {
                return BadRequest();
            }
            await _bookService.Add(viewModel);
            return Ok(viewModel);

        }
        [HttpGet]
        public async Task<IActionResult> Get()
        {
            var view = new GetBooksWithAuthorsView();
            view = await _bookService.Get();
            return Ok(view);
        }
        [HttpPost("{id}")]
        public async Task<IActionResult> Delete(long id)
        {
            if(id == 0)
            {
                return BadRequest();
            }
            await _bookService.Delete(id);
            return Ok();
        }
        [HttpPost]
        public async Task<IActionResult> Update([FromBody]UpdateBookView view)
        {
            if(view == null)
            {
                return BadRequest();
            }
            await _bookService.Update(view);
            return Ok(view);
        }

    }
}
