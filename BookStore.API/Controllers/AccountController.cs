﻿using BookStore.Services.Helpers.Interfaces;
using BookStore.Services.Services.Interfaces;
using BookStore.Services.Views.UserViews;
using IdentityProject.ViewModels;
using Microsoft.AspNetCore.Mvc;
using System.Threading.Tasks;

namespace BookStore.API.Controllers
{
    [Route("api/[controller]/[action]")]
    public class AccountController : Controller
    {
        private readonly IUserService _userService;
        private readonly IRenderHelper<ConfirmEmailView> _renderHelper;
        private readonly IMailHelper _mailHelper;

        public AccountController(IUserService userService, IRenderHelper<ConfirmEmailView> renderHelper, IMailHelper mailHelper)
        {
            _userService = userService;
            _renderHelper = renderHelper;
            _mailHelper = mailHelper;
        }
        [HttpPost]
        public async Task<IActionResult> Register([FromBody] RegisterUserView registerUserView)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest();
            }
            var result = await _userService.Register(registerUserView);
            string emailToken = result.Item1;
            string userId = result.Item2;
            string callBackUrl = Url.Action(nameof(ConfirmEmail), 
                                            "Account", 
                                            new { UserId = userId, Token = emailToken },
                                            protocol: HttpContext.Request.Scheme);
            await SendEmail(registerUserView.UserName, registerUserView.Email, callBackUrl);
            return Ok();
        }

        [HttpPost]
        public async Task<IActionResult> Login([FromBody] LoginView loginView)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest();
            }
            string token = await _userService.Login(loginView);

            return Ok(token);
        }

        public async Task<IActionResult> SendEmail(string recipientName, string recipientAddress, string callBackUrl)
        {

            var viewModel = new ConfirmEmailView
            {
                UserName = recipientName,
                ConfirmationLink = callBackUrl
            };
            var content = await _renderHelper.ToStringAsync("ConfirmEmailFormView", viewModel);

            await _mailHelper.SendMessage(recipientName, recipientAddress, content);

            return Ok();
        }
        [HttpGet]
        public async Task<IActionResult> ConfirmEmail(string userId, string token)
        {
            if (userId == null || token == null)
            {
                return BadRequest("Error");
            }
            var user = await _userService.FindById(userId);
            if (user == null)
            {
                return NotFound("Not found");
            }
            var result = await _userService.ConfirmEmailAsync(user, token);
            return Content(result.Succeeded ? "Confirmed" : "Not confirmed");
        }
    }
}
