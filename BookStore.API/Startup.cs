﻿using BookStore.Services.Helpers;
using BookStore.Services.Helpers.Interfaces;
using BookStore.Services.Services;
using BookStore.Services.Services.Interfaces;
using IdentityProject.ViewModels;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;

namespace BookStore.API
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }
        public void ConfigureServices(IServiceCollection services)
        {
            var connectionString = Configuration.GetConnectionString("DefaultConnection");

            services.AddMvc().SetCompatibilityVersion(CompatibilityVersion.Version_2_2);

            Services.Startup.ConfigureDatabase(services, connectionString);
            Services.Startup.ConfigureIdentity(services);

            services.AddScoped<IUserService, UserService>();
            services.AddScoped<IRenderHelper<ConfirmEmailView>, RenderHelper<ConfirmEmailView>>();
            services.AddScoped<IMailHelper, MailHelper>(i =>
               new MailHelper(
                  Configuration["MailService:Host"],
                  Configuration.GetValue<int>("MailService:Port"),
                  Configuration.GetValue<bool>("MailService:EnableSSL"),
                  Configuration["MailService:SenderAddress"],
                  Configuration["MailService:Password"]));

            services.AddSwaggerGen();
        }

        public void Configure(IApplicationBuilder app, IHostingEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }
            else
            {
                app.UseHsts();
            }
            app.UseAuthentication();

            app.UseHttpsRedirection();

            app.UseSwagger();
            app.UseSwaggerUi();   


            app.UseMvc();
        }
    }
}
