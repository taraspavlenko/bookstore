﻿using BookStore.DataAccess.Entities;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace BookStore.DataAccess.Repositories.Interfaces
{
    public interface IAuthorRepository : IBaseReposiroty<Author>
    {
        Task<Author> GetById(long id);
    }

}
