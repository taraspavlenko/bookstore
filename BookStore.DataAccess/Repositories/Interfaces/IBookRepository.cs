﻿using BookStore.DataAccess.Entities;
using System.Threading.Tasks;

namespace BookStore.DataAccess.Repositories.Interfaces
{
    public interface IBookRepository : IBaseReposiroty<Book>
    {
        Task<Book> GetById(long id);
        Task<long> ReturnId(Book book);
    }
}
