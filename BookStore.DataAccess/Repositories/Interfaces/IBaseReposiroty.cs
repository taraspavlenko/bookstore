﻿using System.Collections.Generic;
using System.Threading.Tasks;

namespace BookStore.DataAccess.Repositories.Interfaces
{
    public interface IBaseReposiroty<TEntity> where TEntity : class
    {
        Task Create(TEntity item);
        Task<IEnumerable<TEntity>> GetAll();
        Task Update(TEntity item);
        Task Delete(TEntity item);
        Task Save();
    }
}
