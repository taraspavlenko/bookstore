﻿using BookStore.DataAccess.Contexts;
using BookStore.DataAccess.Entities;
using BookStore.DataAccess.Repositories.BaseRepository;
using BookStore.DataAccess.Repositories.Interfaces;
using Microsoft.EntityFrameworkCore;
using System.Linq;
using System.Threading.Tasks;

namespace BookStore.DataAccess.Repositories.EntityFrameworkRepositories
{
    public class BookEFRepository : BaseEFRepository<Book> , IBookRepository

    {
        private ApplicationContext _dbContext;
        public BookEFRepository(ApplicationContext dbContext)
            :base(dbContext)
        {
            _dbContext = dbContext;
        }

        public async Task<Book> GetById(long id)
        {
            var item = await _dbContext.Books
               .Where(x => x.Id == id)
               .FirstOrDefaultAsync();
            return item;
        }
        public async Task<long> ReturnId(Book book)
        {
            var id = await _dbContext.Books
                .Where(x => x.Title == book.Title)
                .Select(y => y.Id)
                .FirstOrDefaultAsync();
            return id;
        }
    }
}
