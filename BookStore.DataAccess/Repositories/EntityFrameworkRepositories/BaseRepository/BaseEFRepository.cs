﻿using BookStore.DataAccess.Contexts;
using BookStore.DataAccess.Repositories.Interfaces;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace BookStore.DataAccess.Repositories.BaseRepository
{
    public class BaseEFRepository<TEntity> : IBaseReposiroty<TEntity> where TEntity : class
    {
        private ApplicationContext _dbContext = null;
        private DbSet<TEntity> _tableEntity = null;
        public BaseEFRepository(ApplicationContext dbContext)
        {
            _dbContext = dbContext;
            _tableEntity = _dbContext.Set<TEntity>();
        }

        public async Task<IEnumerable<TEntity>> GetAll()
        {
            return await _tableEntity.ToListAsync();
        }

        public async Task Delete(TEntity item)
        {
            _tableEntity.Remove(item);
            await _dbContext.SaveChangesAsync();
        }

        public async Task Create(TEntity item)
        {
            await _tableEntity.AddAsync(item);
            await _dbContext.SaveChangesAsync();
        }

        public async Task Update(TEntity item)
        {
            _tableEntity.Update(item);
            await _dbContext.SaveChangesAsync();
        }

        public async Task Save()
        {
            await _dbContext.SaveChangesAsync();
        }
    }
}
