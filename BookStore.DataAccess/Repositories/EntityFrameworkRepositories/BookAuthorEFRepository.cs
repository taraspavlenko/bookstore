﻿using BookStore.DataAccess.Contexts;
using BookStore.DataAccess.Entities;
using BookStore.DataAccess.Repositories.BaseRepository;
using BookStore.DataAccess.Repositories.Interfaces;

namespace BookStore.DataAccess.Repositories.EntityFrameworkRepositories
{
    public class BookAuthorEFRepository : BaseEFRepository<BookAuthor>, IBookAuthorRepository
    {
        private ApplicationContext _dbContext;
        public BookAuthorEFRepository(ApplicationContext dbContext)
            : base(dbContext)
        {
            _dbContext = dbContext;
        }
    }
}