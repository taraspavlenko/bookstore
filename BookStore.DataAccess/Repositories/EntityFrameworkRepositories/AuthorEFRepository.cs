﻿using BookStore.DataAccess.Contexts;
using BookStore.DataAccess.Entities;
using BookStore.DataAccess.Repositories.BaseRepository;
using BookStore.DataAccess.Repositories.Interfaces;
using Microsoft.EntityFrameworkCore;
using System.Linq;
using System.Threading.Tasks;

namespace BookStore.DataAccess.Repositories.EntityFrameworkRepositories
{
    public class AuthorEFRepository : BaseEFRepository<Author>, IAuthorRepository
    {
        private ApplicationContext _dbContext;
        public AuthorEFRepository(ApplicationContext dbContext)
            : base(dbContext)
        {
            _dbContext = dbContext;
        }
        public async Task<Author> GetById(long id)
        {
            var item = await _dbContext.Authors
                .Where(x => x.Id == id)
                .FirstOrDefaultAsync();
            return item;
        }
    }
}
