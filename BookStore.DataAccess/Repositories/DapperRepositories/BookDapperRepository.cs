﻿using System.Threading.Tasks;
using BookStore.DataAccess.Entities;
using BookStore.DataAccess.Repositories.DapperRepositories.BaseRepository;
using BookStore.DataAccess.Repositories.Interfaces;

namespace BookStore.DataAccess.Repositories.DapperRepositories
{
    class BookDapperRepository : BaseDapperRepository<Book>, IBookRepository
    {
        private string _connectionString;
        public BookDapperRepository(string connectionString)
            : base("books", connectionString)
        {
            _connectionString = connectionString;
        }

        public Task<Author> GetById(long id)
        {
            throw new System.NotImplementedException();
        }

        public Task<long> ReturnId(Book book)
        {
            throw new System.NotImplementedException();
        }

        public Task Update(Author item)
        {
            throw new System.NotImplementedException();
        }

        Task<Book> IBookRepository.GetById(long id)
        {
            throw new System.NotImplementedException();
        }
    }
}
