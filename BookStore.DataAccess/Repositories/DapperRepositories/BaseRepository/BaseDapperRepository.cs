﻿using BookStore.DataAccess.Repositories.Interfaces;
using Dapper;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Threading.Tasks;

namespace BookStore.DataAccess.Repositories.DapperRepositories.BaseRepository
{
    class BaseDapperRepository<TEntity> : IBaseReposiroty<TEntity> where TEntity : class
    {
        private string _connectionString;
        private string _tableName;
        public BaseDapperRepository(string tableName, string connectionString)
        {
            _tableName = tableName;
            _connectionString = connectionString;
        }

        public Task Create(TEntity item)
        {
            throw new NotImplementedException();
        }

        public Task Delete(TEntity item)
        {
            throw new NotImplementedException();
        }

        public Task<IEnumerable<TEntity>> GetAll()
        {
            throw new NotImplementedException();
        }

        public Task Save()
        {
            throw new NotImplementedException();
        }

        public Task Update(TEntity item)
        {
            throw new NotImplementedException();
        }
    }
}
