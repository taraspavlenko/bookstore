﻿using System.Collections.Generic;

namespace BookStore.DataAccess.Entities
{
    public class Order : BaseEntity
    {
        public int Quantity { get; set; }
        public double UnitPrice { get; set; }
        public double Total { get; set; }

        public virtual List<BookOrder> BookOrders { get; set; }
    }
}
