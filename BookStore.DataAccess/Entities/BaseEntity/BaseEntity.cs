﻿using System;

namespace BookStore.DataAccess.Entities
{
    public class BaseEntity
    {
        public BaseEntity()
        {
            CreationDate = DateTime.Now;
        }
        public long Id { get; set; }
        public DateTime CreationDate { get; private set; }
        public DateTime? UpdationDate { get; set; }
    }
}