﻿namespace BookStore.DataAccess.Entities
{
    public class BookOrder : BaseEntity
    {
        public long BookId { get; set; }
        public long OrderId { get; set; }
        public Book Book { get; set; }
        public Order Order { get; set; }
    }
}
