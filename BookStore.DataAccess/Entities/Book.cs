﻿using System.Collections.Generic;

namespace BookStore.DataAccess.Entities
{
    public class Book : BaseEntity
    {
        public string Title { get; set; }
        public virtual List<BookAuthor> BookAuthors { get; set; }
        public virtual List<BookOrder> BookOrders { get; set; }

    }
}
