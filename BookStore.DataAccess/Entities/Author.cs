﻿using System.Collections.Generic;

namespace BookStore.DataAccess.Entities
{
    public class Author : BaseEntity
    {
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public virtual List<BookAuthor> BookAuthors { get; set; }

    }
}
