﻿using BookStore.DataAccess.Contexts;
using BookStore.DataAccess.Entities;
using BookStore.DataAccess.Repositories.EntityFrameworkRepositories;
using BookStore.DataAccess.Repositories.Interfaces;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;
namespace BookStore.DataAccess
{
    public class Startup
    {
        public static void ConfigureDatabase(IServiceCollection services, string connectionString)
        {
            services.AddScoped<IBookRepository, BookEFRepository>();
            services.AddScoped<IBookAuthorRepository, BookAuthorEFRepository>();
            services.AddScoped<IAuthorRepository, AuthorEFRepository>();

            services.AddDbContext<ApplicationContext>(options =>
                options.UseSqlServer(
                    connectionString,
                        b => b.MigrationsAssembly("BookStore.DataAccess")));
        }
    }
}
