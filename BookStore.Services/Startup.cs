﻿using BookStore.DataAccess.Contexts;
using BookStore.DataAccess.Entities;
using BookStore.Services.Helpers;
using BookStore.Services.Helpers.Interfaces;
using BookStore.Services.Services;
using BookStore.Services.Services.Interfaces;
using IdentityProject.ViewModels;
using Microsoft.AspNetCore.Identity;
using Microsoft.Extensions.DependencyInjection;

namespace BookStore.Services
{
    public static class Startup
    {
        public static void ConfigureDatabase(IServiceCollection services, string connectionString)
        {
            services.AddScoped<IBookService, BookService>();
            services.AddScoped<IAuthorService, AuthorService>();
            services.AddScoped<IBookAuthorService, BookAuthorService>();


            DataAccess.Startup.ConfigureDatabase(services, connectionString);
        }
        public static void ConfigureIdentity(IServiceCollection services)
        {
            var builder = services.AddIdentity<User, IdentityRole>(o =>
            {
                o.Password.RequireDigit = true;
                o.Password.RequireLowercase = true;
                o.Password.RequireUppercase = true;
                o.Password.RequireNonAlphanumeric = false;
                o.Password.RequiredLength = 6;
            });
            builder = new IdentityBuilder(builder.UserType, typeof(IdentityRole), builder.Services);
            builder.AddEntityFrameworkStores<ApplicationContext>().AddDefaultTokenProviders();
        }
    }
}
