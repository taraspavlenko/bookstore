﻿using System.Threading.Tasks;

namespace BookStore.Services.Helpers.Interfaces
{
    public interface IRenderHelper<T> where T : class
    {
        Task<string> ToStringAsync(string viewName, T model);
    }
}
