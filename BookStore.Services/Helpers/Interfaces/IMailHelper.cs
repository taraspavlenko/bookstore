﻿using System.Threading.Tasks;

namespace BookStore.Services.Helpers.Interfaces
{
    public interface IMailHelper
    {
        Task SendMessage(string recipientName, string recipientAddress, string content);
    }
}
