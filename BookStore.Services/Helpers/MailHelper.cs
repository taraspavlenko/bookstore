﻿using BookStore.Services.Helpers.Interfaces;
using MailKit.Net.Smtp;
using MimeKit;
using MimeKit.Text;
using System.Threading.Tasks;

namespace BookStore.Services.Helpers
{
    public class MailHelper : IMailHelper
    {
        private string _host;
        private int _port;
        private bool _enableSSL;
        private string _senderAddress;
        private string _password;

        public MailHelper(string host, int port, bool enableSSL, string senderAddress, string password)
        {
            _host = host;
            _port = port;
            _enableSSL = enableSSL;
            _senderAddress = senderAddress;
            _password = password;
        }
        public async Task SendMessage(string recipientName, string recipientAddress, string content)
        {
            var message = new MimeMessage();
            message.From.Add(new MailboxAddress("BookStore", _senderAddress));
            message.To.Add(new MailboxAddress(recipientName, recipientAddress));
            message.Subject = "Confirm your email address";
            message.Body = new TextPart(TextFormat.Html)
            {
                Text = content
            };

            using (var client = new SmtpClient())
            {
                await client.ConnectAsync(_host, _port, _enableSSL);
                await client.AuthenticateAsync(_senderAddress, _password);
                await client.SendAsync(message);
                await client.DisconnectAsync(true);
            }
        }
    }
}
