﻿using BookStore.DataAccess.Entities;
using BookStore.Services.Services.Interfaces;
using BookStore.Services.Views.UserViews;
using Microsoft.AspNetCore.Identity;
using System;
using System.Linq;
using System.Threading.Tasks;

namespace BookStore.Services.Services
{
    public class UserService : IUserService
    {
        private readonly UserManager<User> _userManager;
        private readonly SignInManager<User> _signInManager;

        public UserService(UserManager<User> userManager, SignInManager<User> signInManager)
        {
            _userManager = userManager;
            _signInManager = signInManager;

        }

        public async Task<IdentityResult> ConfirmEmailAsync(User user, string token)
        {
            return await _userManager.ConfirmEmailAsync(user, token);
        }

        public async Task<User> FindById(string userId)
        {
            if (string.IsNullOrEmpty(userId))
            {
                throw new Exception("User id is null");
            }
            var user = await _userManager.FindByIdAsync(userId);
            if (user == null)
            {
                throw new Exception("Not found");
            }
            return user;
        }

        public async Task<(string, string)> Register(RegisterUserView registerUserView)
        {
            User user = await _userManager.FindByEmailAsync(registerUserView.Email);
            if (user != null)
            {
                throw new Exception("Current user already exists");
            }
            user = new User
            {
                UserName = registerUserView.UserName,
                Email = registerUserView.Email
            };
            IdentityResult result = await _userManager.CreateAsync(user, registerUserView.Password);
            if (!result.Succeeded)
            {
                var errors = result.Errors.ToList();
                throw new Exception("Errors during a user creation");
            }
            string emailToken = await _userManager.GenerateEmailConfirmationTokenAsync(user);
            return (emailToken, user.Id);
        }
        public async Task<string> Login(LoginView loginView)
        {
            User user = await _userManager.FindByEmailAsync(loginView.Email);
            if(user == null)
            {
                throw new Exception("You are not registered yet");
            }
            await _signInManager.SignInAsync(user, loginView.IsPersistent);

            return "It's will be JWT";
        }
    }
}
