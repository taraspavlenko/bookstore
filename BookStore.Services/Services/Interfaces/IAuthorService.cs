﻿using System.Threading.Tasks;
using TestDbMetanit.Views.AuthorViews;

namespace BookStore.Services.Services.Interfaces
{
    public interface IAuthorService
    {
        Task Add(CreateAuthorView view);
        Task<GetAuthorsView> Get();
        Task<GetAuthorView> Get(long id);
        Task Update(UpdateAuthorView view);
        Task Delete(long id);
    }
}
