﻿using BookStore.Services.Services.Views.BookViews;
using BookStore.Services.Views.BookViews;
using System.Threading.Tasks;

namespace BookStore.Services.Services.Interfaces
{
    public interface IBookService
    {
        Task Add(CreateBookView view);
        Task<GetBooksWithAuthorsView> Get();
        Task<GetBooksWithAuthorsView> GetById(long id);
        Task<UpdateBookView> Update(UpdateBookView view);
        Task Delete(long id);
    }
}
