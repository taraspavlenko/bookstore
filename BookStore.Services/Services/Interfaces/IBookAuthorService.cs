﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace BookStore.Services.Services.Interfaces
{
    public interface IBookAuthorService
    {
        Task Create(long bookId, long[] authorsId);
        Task Update(long bookId, long[] authorsId);
    }
}
