﻿using BookStore.DataAccess.Entities;
using BookStore.Services.Views.UserViews;
using Microsoft.AspNetCore.Identity;
using System.Threading.Tasks;

namespace BookStore.Services.Services.Interfaces
{
    public interface IUserService
    {
        Task<(string, string)> Register(RegisterUserView registerUserView);
        Task<User> FindById(string userId);
        Task<IdentityResult> ConfirmEmailAsync(User user, string token);
        Task<string> Login(LoginView loginView);
    }
}
