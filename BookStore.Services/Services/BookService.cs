﻿using BookStore.DataAccess.Entities;
using BookStore.DataAccess.Repositories.Interfaces;
using BookStore.Services.Services.Interfaces;
using BookStore.Services.Services.Views.BookViews;
using BookStore.Services.Views.BookViews;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace BookStore.Services.Services
{
    public class BookService : IBookService
    {
        private IBookRepository _bookRepository;
        private IBookAuthorRepository _bookAuthorRepository;
        private IAuthorRepository _authorRepository;
        private IBookAuthorService _bookAuthorService;
        public BookService(IBookRepository bookRepository, IBookAuthorRepository bookAuthorRepository, IAuthorRepository authorRepository, IBookAuthorService bookAuthorService)
        {
            _bookRepository = bookRepository;
            _bookAuthorRepository = bookAuthorRepository;
            _authorRepository = authorRepository;
            _bookAuthorService = bookAuthorService;
        }

        public async Task Add(CreateBookView view)
        {
            var book = new Book
            {
                Title = view.Title
            };
            await _bookRepository.Create(book);

            //var bookId = await _bookRepository.ReturnId(book);

            await _bookAuthorService.Create(book.Id, view.AuthorsId);
            await _bookRepository.Save();
        }
        public async Task<GetBooksWithAuthorsView> Get()
        {
            var bookView = new GetBooksWithAuthorsView();

            var books = await _bookRepository.GetAll();
            var bookAuthors = await _bookAuthorRepository.GetAll();
            var authorts = await _authorRepository.GetAll();

            bookView.Books.AddRange(
           books.Select(x => new BookGetBooksWithAuthorsViewItem
           {
               Id = x.Id,
               Title = x.Title,
               CreationDate = x.CreationDate,
               UpdationDate = x.UpdationDate,
               Authors = bookAuthors.Where(y => y.BookId == x.Id).Select(ob => new AuthorGetBooksWithAuthorsViewItem
               {
                   Id = ob.Author.Id,
                   FirstName = ob.Author.FirstName,
                   LastName = ob.Author.LastName,
                   CreationDate = ob.Author.CreationDate,
                   UpdationDate = ob.Author.UpdationDate
               }).ToList()
           }).ToList());

            return bookView;
        }
        public async Task<GetBooksWithAuthorsView> GetById(long id)
        {
            var bookView = new GetBooksWithAuthorsView();

            //var book = await _bookRepository.GetById(id);
            //var bookAuthors = await _bookAuthorRepository.GetAll();

            //bookView.Books.Add(new BookGetBooksWithAuthorsViewItem
            //{
            //    Id = book.Id,
            //    Authors = bookAuthors.Where(x => x.BookId == book.Id).Select(author => new AuthorGetBooksWithAuthorsViewItem
            //    {
            //        Id = author.Author.Id,
            //        FirstName = author.Author.FirstName,
            //        LastName = author.Author.LastName
            //    }).ToList()
            //});
            return bookView;
        }

        public async Task<UpdateBookView> Update(UpdateBookView view)
        {
            var book = await _bookRepository.GetById(view.Id);

            book.Title = view.Title;

            await _bookAuthorService.Update(view.Id, view.AuthorsId);
            return new UpdateBookView();
        }
        public async Task Delete(long id)
        {
            var bookToDelete = await _bookRepository.GetById(id);
            if (bookToDelete != null)
            {
                await _bookRepository.Delete(bookToDelete);
            }
        }
    }
}

