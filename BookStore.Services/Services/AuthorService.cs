﻿using BookStore.DataAccess.Entities;
using BookStore.DataAccess.Repositories.Interfaces;
using BookStore.Services.Services.Interfaces;
using System;
using System.Linq;
using System.Threading.Tasks;
using TestDbMetanit.Views.AuthorViews;

namespace BookStore.Services.Services
{
    public class AuthorService : IAuthorService
    {
        private IBookRepository _bookRepository;
        private IBookAuthorRepository _bookAuthorRepository;
        private IAuthorRepository _authorRepository;

        public AuthorService(IBookRepository bookRepository, IBookAuthorRepository bookAuthorRepository, IAuthorRepository authorRepository)
        {
            _bookRepository = bookRepository;
            _bookAuthorRepository = bookAuthorRepository;
            _authorRepository = authorRepository;
        }
        public async Task Add(CreateAuthorView view)
        {
            var author = new Author
            {
                FirstName = view.FirstName,
                LastName = view.LastName
            };
            await _authorRepository.Create(author);
        }
        public async Task<GetAuthorsView> Get()
        {
            var authorView = new GetAuthorsView();

            var authors = await _authorRepository.GetAll();

            authorView.Authors.AddRange(
                authors.Select(x => new AuthorGetAuthorsViewItem
                {
                    Id = x.Id,
                    FirstName = x.FirstName,
                    LastName = x.LastName,
                    CreationDate = x.CreationDate,
                    UpdationDate = x.UpdationDate
                }).ToList());

            return authorView;
        }
        public async Task<GetAuthorView> Get(long id)
        {
            var author = await _authorRepository.GetById(id);
            if (author != null)
            {
                var authorView = new GetAuthorView
                {
                    Id = author.Id,
                    FirstName = author.FirstName,
                    LastName = author.LastName,
                    CreationDate = author.CreationDate,
                    UpdationDate = author.UpdationDate
                };
                return authorView;
            }
            return null;
        }

        public async Task Update(UpdateAuthorView view)
        {
            var author = await _authorRepository.GetById(view.Id);

            author.FirstName = view.FirstName;
            author.LastName = view.LastName;
            author.UpdationDate = DateTime.Now;

            await _authorRepository.Update(author);
        }
        public async Task Delete(long id)
        {
            var itemToDelete = await _authorRepository.GetById(id);
            if (itemToDelete != null)
            {
                await _authorRepository.Delete(itemToDelete);
            }
        }
    }
}
