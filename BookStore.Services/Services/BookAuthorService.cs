﻿using BookStore.DataAccess.Entities;
using BookStore.DataAccess.Repositories.Interfaces;
using BookStore.Services.Services.Interfaces;
using System.Threading.Tasks;

namespace BookStore.Services.Services
{
    class BookAuthorService : IBookAuthorService
    {
        private readonly IBookAuthorRepository _bookAuthorRepository;
        public BookAuthorService(IBookAuthorRepository bookAuthorRepository)
        {
            _bookAuthorRepository = bookAuthorRepository;
        }
        public async Task Create(long bookId, long[] authorsId)
        {
            if (bookId != 0 && authorsId.Length != 0)
            {
                foreach (var authorId in authorsId)
                {
                    var bookAuthor = new BookAuthor
                    {
                        BookId = bookId,
                        AuthorId = authorId
                    };
                    await _bookAuthorRepository.Create(bookAuthor);
                }
            }
        }

        public async Task Update(long bookId, long[] authorsId)
        {
            var booksAuthors = await _bookAuthorRepository.GetAll();
            

        }
    }
}
