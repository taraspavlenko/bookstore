﻿namespace IdentityProject.ViewModels
{
    public class ConfirmEmailView
    {
        public string UserName { get; set; }
        public string ConfirmationLink { get; set; }
    }
}
