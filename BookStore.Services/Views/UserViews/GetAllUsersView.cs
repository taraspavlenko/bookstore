﻿using System;
using System.Collections.Generic;
using System.Text;

namespace BookStore.Services.Views.UserViews
{
    public class GetAllUsersView
    {
        public GetAllUsersView()
        {
            Users = new List<UserGetAllUsersItemView>();
        }
        public List<UserGetAllUsersItemView> Users { get; set; }
    }

    public class UserGetAllUsersItemView
    {
        public string Id { get; set; }
        public string UserNamer { get; set; }
        public string Email { get; set; }
    }
}
