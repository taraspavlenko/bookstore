﻿using System;
using System.Collections.Generic;

namespace TestDbMetanit.Views.AuthorViews
{
    public class GetAuthorsView
    {
        public List<AuthorGetAuthorsViewItem> Authors { get; set; }
        public GetAuthorsView()
        {
            Authors = new List<AuthorGetAuthorsViewItem>();
        }
    }

    public class AuthorGetAuthorsViewItem
    {
        public long Id { get; set; }
        public string FirstName{ get; set; }
        public string LastName{ get; set; }
        public DateTime CreationDate { get; set; }
        public DateTime? UpdationDate { get; set; }
    }
}
