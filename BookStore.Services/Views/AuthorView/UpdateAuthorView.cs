﻿namespace TestDbMetanit.Views.AuthorViews
{
    public class UpdateAuthorView
    {
        public int Id { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
    }
}
