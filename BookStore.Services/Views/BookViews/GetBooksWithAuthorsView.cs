﻿using System;
using System.Collections.Generic;

namespace BookStore.Services.Services.Views.BookViews
{
    public class GetBooksWithAuthorsView
    {
        public GetBooksWithAuthorsView()
        {
            Books = new List<BookGetBooksWithAuthorsViewItem>();
        }
        public List<BookGetBooksWithAuthorsViewItem> Books { get; set; }
    }

    public class BookGetBooksWithAuthorsViewItem
    {
        public BookGetBooksWithAuthorsViewItem()
        {
            Authors = new List<AuthorGetBooksWithAuthorsViewItem>();
        }
        public long Id { get; set; }
        public string Title { get; set; }
        public DateTime CreationDate { get; set; }
        public DateTime? UpdationDate { get; set; }
        public List<AuthorGetBooksWithAuthorsViewItem> Authors { get; set; }
    }

    public class AuthorGetBooksWithAuthorsViewItem
    {
        public long Id { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public DateTime CreationDate { get; set; }
        public DateTime? UpdationDate { get; set; }
    }
}
