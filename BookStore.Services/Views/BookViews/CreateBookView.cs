﻿namespace BookStore.Services.Views.BookViews
{
    public class CreateBookView
    {
        public string Title { get; set; }
        public long[] AuthorsId { get; set; }
    }
}
