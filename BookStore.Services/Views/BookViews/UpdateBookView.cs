﻿namespace BookStore.Services.Views.BookViews
{
    public class UpdateBookView
    {
        public long Id { get; set; }
        public string Title { get; set; }
        public long[] AuthorsId { get; set; }
    }
}
