﻿using System;
using System.Collections.Generic;

namespace BookStore.Services.Views.BookViews
{
    public class GetBooksView
    {
        public GetBooksView()
        {
            Books = new List<BookGetOnlyBooksViewItem>();
        }
        public List<BookGetOnlyBooksViewItem> Books { get; set; }
    }
    public class BookGetOnlyBooksViewItem
    {
        public long Id { get; set; }
        public string Title { get; set; }
        public DateTime CreationDate { get; set; }
        public DateTime? UpdationDate { get; set; }

    }
}
